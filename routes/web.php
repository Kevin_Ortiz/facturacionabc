<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
   

    Route::get('/home', 'HomeController@index')->name('home');

    /* RUTAS facturas */
    Route::resource('EmitirFactura' , 'facturacion');
    
    /* Rutas configuracion */
    Route::get('/configuracion',  'Man_configuracion@index');
    Route::put('/configuracion/updateConfig/{id}', 'Man_configuracion@update');

    /* RUTAS clientes */
    Route::get('/clientes', 'Man_clientes@index');
    Route::post('/clientes/store', 'Man_clientes@store');
    Route::put('/clientes/updateClientes/{id}', 'Man_clientes@update');
    Route::put('/clientes/deleteClientes/{id}', 'Man_clientes@destroy'); 

    /* RUTAS empresas */
    Route::get('/empresas', 'Man_empresas@index');
    Route::post('/empresas/store', 'Man_empresas@store');
    Route::put('/empresas/updateEmpresas/{id}', 'Man_empresas@update');
    Route::put('/empresas/deleteEmpresas/{id}', 'Man_empresas@destroy'); 

    /* RUTAS productos */
    Route::get('/productos' , 'Man_ProductosController@index');
    Route::post('/store', 'Man_ProductosController@store');
    Route::put('/productos/updateProduct/{id}', 'Man_ProductosController@update');
    Route::put('/productos/deleteProduct/{id}', 'Man_ProductosController@destroy'); 


});
