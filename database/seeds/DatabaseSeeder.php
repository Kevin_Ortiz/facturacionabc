<?php

use App\Cliente;
use App\Configuracion;
use App\Empresa;
use App\Producto;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(Role::class, 1)->create(['name' => 'admin']);
        factory(Role::class, 1)->create(['name' => 'user']);


        factory(User::class, 1)->create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('1234'),
            'role_id' => Role::ADMIN
        ]);

        factory(User::class, 1)->create([
            'name' => 'user',
            'email' => 'user@user.com',
            'password' => bcrypt('1234'),
            'role_id' => Role::USER
        ]);

        Configuracion::create([
            'establecimiento' => 'Credit.sa',
            'punto_emision' => 'Matriz Alborada 5 Etapa',
            'sec_factura' => '00001',
        ]);

        factory(Producto::class, 7)->create();
        factory(Empresa::class, 5)->create();
        factory(Cliente::class, 6)->create();
      
    }
}
