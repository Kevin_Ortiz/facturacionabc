<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Empresa;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Empresa::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'ruc' => $faker->unique()->randomNumber($nbDigits = 9),

    ];
});
