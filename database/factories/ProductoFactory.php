<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Producto;
use Faker\Generator as Faker;

$factory->define(Producto::class, function (Faker $faker) {
    return [

        'codigo' => $faker->unique()->randomNumber($nbDigits = 5),
        'name' =>  $faker->streetName,
        'precio' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 5),
    ];
});
