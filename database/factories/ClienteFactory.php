<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Cliente;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Cliente::class, function (Faker $faker) {
    return [

        'cedula' => $faker->unique()->randomNumber($nbDigits = 9),
        'ruc' => $faker->unique()->randomNumber($nbDigits = 9),
        'name' => $faker->name,
    ];
});
