<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Producto
 *
 * @property int $id
 * @property int $codigo
 * @property int $fac_detalle_id
 * @property string $name
 * @property int $precio
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Producto newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Producto newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Producto query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Producto whereCodigo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Producto whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Producto whereFacDetalleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Producto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Producto whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Producto wherePrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Producto whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Producto extends Model
{
    protected $table='productos';
    protected $fillable=['name', 'precio'];
    
    function factura_detalle(){
        return $this->hasOne(Factura_detalle::class);
    }
}
