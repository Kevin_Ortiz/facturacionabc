<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empresa;

class Man_empresas extends Controller
{
    public function index()
    {
        $empresas = Empresa::all();
        return view('view_empresa.empresas',compact('empresas'));
    }

    public function store(Request $request)
    {
        $empresas = new Empresa;
        
        $empresas->name = $request->input('name');
        $empresas->ruc = $request->input('ruc');
        $empresas->save();
    }

    public function update(Request $request ,$id)
    {
        $empresas = Empresa::find($id);
        
        $empresas->name = $request->input('name');
        $empresas->ruc = $request->input('ruc');
        $empresas->save();
    }

    public function destroy(Request $request , $id)
    {
        Empresa::destroy($id);
       
    }
      
}
