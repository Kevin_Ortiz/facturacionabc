<?php

namespace App\Http\Controllers;

use App\Cliente;
use Doctrine\DBAL\Schema\Index;
use Illuminate\Http\Request;
use App\Http\Requests\ClienteRequest;

class Man_clientes extends Controller
{
    public function index()
    {
        $cliente = Cliente::all();
       return view('view_clientes.clientes',compact('cliente'));
    }

    public function store(ClienteRequest $request)
    {
      $cliente = new Cliente;
      $cliente->cedula = $request->input('cedula');
      $cliente->ruc = $request->input('ruc');
      $cliente->name = $request->input('name');
      $cliente->save();  
    }

    public function update(Request $request ,$id)
    {
        $cliente = Cliente::find($id);
        
        $cliente->cedula = $request->input('cedula');
        $cliente->ruc = $request->input('ruc');
        $cliente->name = $request->input('name');
        $cliente->save();
    }

    public function destroy(Request $request , $id)
    {
        Cliente::destroy($id);
       
    }
}
