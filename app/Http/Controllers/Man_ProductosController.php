<?php

namespace App\Http\Controllers;
use App\Producto;
use Illuminate\Http\Request;

class Man_ProductosController extends Controller
{
    public function index()
    {
        $productos = Producto::all();
       return view('view_productos.productos',compact('productos'));
    }

    
    public function create()
    {
        return view('view_productos.create');
    }

    
    public function store(Request $request)
    {
        $productos = new Producto;
        $productos->codigo = $request->input('codigo');
        $productos->name = $request->input('name');
        $productos->precio = $request->input('precio');
        $productos->save();
    }

    public function update(Request $request ,$id)
    {
        $productos = Producto::find($id);
        $productos->codigo = $request->input('codigo');
        $productos->name = $request->input('name');
        $productos->precio = $request->input('precio');
        $productos->save();
    }

    public function destroy(Request $request , $id)
    {
        Producto::destroy($id);
       
    }
}
