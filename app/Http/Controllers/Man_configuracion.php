<?php

namespace App\Http\Controllers;

use App\Configuracion;
use Illuminate\Http\Request;

class Man_configuracion extends Controller
{
    public function index()
    {
       $config = Configuracion::all();
       return view('view_configuracion.config',compact('config'));
    }

    public function update(Request $request ,$id)
    {
        $config = Configuracion::find($id);
        $config->establecimiento = $request->input('establecimiento');
        $config->punto_emision = $request->input('punto_emision');
        $config->sec_factura = $request->input('sec_factura');
        $config->save();
    }
}
