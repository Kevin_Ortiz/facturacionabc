<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cedula' => 'min:10|required|unique:clientes',
            'ruc' => 'min:12|required|unique:clientes',
            'name' => 'min:4|max:20|required|string',
        ];
    }
   /*  public function messages()
    {
        return response()->json([
            'cedula.unique' => 'Numero de cedula ya se encuentra resgistrado'
        ]);
    } */
}
