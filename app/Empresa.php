<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Empresa
 *
 * @property int $id
 * @property string $name
 * @property int $ruc
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Empresa newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Empresa newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Empresa query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Empresa whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Empresa whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Empresa whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Empresa whereRuc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Empresa whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Empresa extends Model
{

    protected $table='empresas';
    protected $fillable=['name', 'ruc'];

    function factura(){
        return $this->hasOne(Factura::class);
    }
}
