<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Configuracion
 *
 * @property int $id
 * @property string $establecimiento
 * @property string $punto_emision
 * @property string $sec_factura
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Configuracion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Configuracion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Configuracion query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Configuracion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Configuracion whereEstablecimiento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Configuracion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Configuracion wherePuntoEmision($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Configuracion whereSecFactura($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Configuracion whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Configuracion extends Model
{
    protected $table='configuracions';
    protected $fillable=['establecimiento', 'punto_emision', 'sec_factura'];
}
