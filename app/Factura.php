<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Factura
 *
 * @property int $id
 * @property int $user_id
 * @property int $empresa_id
 * @property int $cliente_id
 * @property string $establecimiento
 * @property string $punto_emision
 * @property string $sec_factura
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factura newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factura newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factura query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factura whereClienteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factura whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factura whereEmpresaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factura whereEstablecimiento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factura whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factura wherePuntoEmision($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factura whereSecFactura($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factura whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factura whereUserId($value)
 * @mixin \Eloquent
 */
class Factura extends Model
{
 
}
