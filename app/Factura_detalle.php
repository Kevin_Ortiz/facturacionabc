<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Factura_detalle
 *
 * @property int $id
 * @property int $factura_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factura_detalle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factura_detalle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factura_detalle query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factura_detalle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factura_detalle whereFacturaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factura_detalle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factura_detalle whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Factura_detalle extends Model
{
    //
}
