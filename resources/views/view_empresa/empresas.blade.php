@extends('layouts.app')

@section('content')

<div class="row">

    <div class="col-lg-3"></div>

    <div class="col-lg-8">


        <h1 class="text-center p-3">Mantenimiento Empresas</h1>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModalEmpresas">Nuevo</button>
        <table class="table table-border">
            <thead>
                <tr>
                    <td>id</td>
                    <td>Nombre</td>
                    <td>ruc</td>
                    <td>Acciones</td>
                </tr>
            </thead>
            <tbody>
                @foreach($empresas as $empresas)
                    <tr>
                        <td>{{ $empresas->id }}</td>
                        <td>{{ $empresas->name }}</td>
                        <td>{{ $empresas->ruc }}</td>
                        <td>
                            <a href="#" class="edit-modal btn btn-success" data-id="{{ $empresas->id }}"
                                data-toggle="modal" data-target="#editModalEmpresas">Actulizar</a>

                            <a href="#" class="delete-modal btn btn-danger" data-id="{{ $empresas->id }}"
                                data-toggle="modal" data-target="#deleteModalEmpresas">Eliminar</a>
                        </td>

                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@include('view_empresa.Createmodal');
@include('view_empresa.editmodal');
@include('view_empresa.deletemodal');
@endsection
