<!-- Button trigger modal -->

  <!-- Modal create empresas -->
  <div class="modal fade" id="addModalEmpresas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Registra Nueva Empresa</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
       <form id="addempresa">
            <div class="modal-body">
                {{ csrf_field() }}
                
                <div class="form-group">
                    <label>Nombre</label>
                    <input type="text" class="form-control" name="name" placeholder="ingrese el codigo de la empresa" required>
                </div>

                <div class="form-group">
                    <label>ruc</label>
                    <input type="text" class="form-control" name="ruc" placeholder="ingrese el ruc del la empresa" required>
                </div>

             </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
      </form>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script type="text/javascript">

    $(document).ready(function() {

        $('#addempresa').on('submit', function(e){
            e.preventDefault();

            $.ajax({
                type:"POST",
                url: 'empresas/store',
                data: $('#addempresa').serialize(),
                success: function (response) {
                    console.log(response)
                    $('#addModalEmpresas').modal('hide')
                    alert("Guardado...");
                    location.reload();
                },
                error: function(error){
                    console.log(error)
                    alert("Empresa No Guardado..");
                }
            });
        });
    });
</script>