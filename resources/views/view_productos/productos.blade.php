@extends('layouts.app')

@section('content')


<div class="row">

    <div class="col-lg-3"></div>

    <div class="col-lg-8">

    
            <h1 class="text-center p-3">Mantenimiento de Productos</h1>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModalProductos">Nuevo</button>
            <table class="table table-border">
               <thead>
                   <tr>
                       <td>id</td>
                       <td>codigo</td>
                       <td>Nombre</td>
                       <td>Precio</td>
                       <td>Acciones</td>
                   </tr>
               </thead>
               <tbody>
                   @foreach ($productos as $productos)
                       <tr>
                       <td>{{$productos->id}}</td>
                       <td>{{$productos->codigo}}</td>
                       <td>{{$productos->name}}</td>
                       <td>{{$productos->precio}}</td>
                       <td>
                        <a href="#" class="edit-modal btn btn-success" data-id="{{$productos->id}}" 
                             data-toggle="modal" data-target="#editModalProductos">Actulizar</a> 
                            
                             <a href="#" class="delete-modal btn btn-danger" data-id="{{$productos->id}}" 
                                data-toggle="modal" data-target="#deleteModalProductos">Eliminar</a> 
                       </td>
                       </tr>
                   @endforeach
               </tbody>
            </table>
      
    </div>
    
</div>
@include('view_productos.Createmodal');
@include('view_productos.editmodal');
@include('view_productos.deletemodal');
@endsection