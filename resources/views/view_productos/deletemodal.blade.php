<!-- Button trigger modal -->

  <!-- Modal Productos -->
  <div class="modal fade" id="deleteModalProductos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Eliminar Producto</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
       <form id="deleteporducto">
          
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                
                <div class="modal-body">
                  <input type="hidden" name="id" id="id">
                    <p>Seguro que desea Eliminar este registro...??</p>
                </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Eliminar</button>
            </div>
      </form>
      </div>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script type="text/javascript">
$(document).ready(function() {

  $('.delete-modal').on('click', function(){

    $('#deleteporducto').modal('show');
    $tr = $(this).closest('tr');
      var data = $tr.children("td").map(function(){
          return $(this).text();
      }).get();
      console.log(data);

      $('#id').val(data[0]);
  });

  $('#deleteporducto').on('submit', function(e){
    e.preventDefault();
    var id = $('#id').val();
    $.ajax({
      type: "PUT",
      url:'productos/deleteProduct/'+id,
      data: $('#deleteporducto').serialize(),
      
      success: function(response){
        console.log(response);
        $('#deleteModalProductos').modal('hide');
        alert("Eliminado..");
        location.reload();
      },
      error: function(error){
        console.log(error);
      }
    });
  })
    
});
    
</script>