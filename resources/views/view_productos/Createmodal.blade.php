<!-- Button trigger modal -->

  <!-- Modal -->
  <div class="modal fade" id="addModalProductos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Nuevo Producto</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
       <form id="addporducto">
            <div class="modal-body">
                {{ csrf_field() }}
                 <div class="form-group">
                    <label>Codigo</label>
                    <input type="text" class="form-control" name="codigo" placeholder="ingrese el codigo del producto" required>
                </div>

                <div class="form-group">
                    <label>Nombre</label>
                    <input type="text" class="form-control" name="name" placeholder="ingrese el codigo del producto" required>
                </div>

                <div class="form-group">
                    <label>Precio</label>
                    <input type="text" class="form-control" name="precio" placeholder="ingrese el codigo del producto" required>
                </div>

             </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
      </form>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script type="text/javascript">

    $(document).ready(function() {

        $('#addporducto').on('submit', function(e){
            e.preventDefault();

            $.ajax({
                type:"POST",
                url: "\store",
                data: $('#addporducto').serialize(),
                success: function (response) {
                    console.log(response)
                    $('#addModalProductos').modal('hide')
                    alert("Guardado...");
                    location.reload();
                },
                error: function(error){
                    console.log(error)
                    alert("Producto No Guardado");
                }
            });
        });
    });
</script>