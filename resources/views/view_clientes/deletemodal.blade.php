<!-- Button trigger modal -->

  <!-- Modal clientes -->
  <div class="modal fade" id="deleteModalClientes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Eliminar Cliente</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
       <form id="deleteClientes">
          
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                
                <div class="modal-body">
                  <input type="hidden" name="id" id="id">
                    <p>Seguro que desea Eliminar este registro...??</p>
                </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Eliminar</button>
            </div>
      </form>
      </div>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script type="text/javascript">
$(document).ready(function() {

  $('.delete-modal').on('click', function(){

    $('#deleteClientes').modal('show');
    $tr = $(this).closest('tr');
      var data = $tr.children("td").map(function(){
          return $(this).text();
      }).get();
      console.log(data);

      $('#id').val(data[0]);
  });

  $('#deleteClientes').on('submit', function(e){
    e.preventDefault();
    var id = $('#id').val();
    $.ajax({
      type: "PUT",
      url:'clientes/deleteClientes/'+id,
      data: $('#deleteClientes').serialize(),
      
      success: function(response){
        console.log(response);
        $('#deleteModalClientes').modal('hide');
        alert("Eliminado..");
        location.reload();
      },
      error: function(error){
        console.log(error);
      }
    });
  })
    
});
    
</script>