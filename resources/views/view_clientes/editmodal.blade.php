<!-- Button trigger modal -->

  <!-- Modal productos -->
  <div class="modal fade" id="editModalClientes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Actualizar Clientes</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
       <form id="editClientes">
            <div class="modal-body">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <input type="hidden" name="id" id="id">
                 

                <div class="form-group">
                    <label>Cedula:</label>
                    <input type="text" class="form-control" name="cedula" id="cedula" placeholder="ingrese su numero de cedula" required>
                </div>

                <div class="form-group">
                    <label>ruc:</label>
                    <input type="text" class="form-control" name="ruc" id="ruc" placeholder="ingrese su ruc" required>
                </div>

                <div class="form-group">
                    <label>Nombre:</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="ingrese su nombre" required>
                </div>
             </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
      </form>
      </div>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script type="text/javascript">
$(document).ready(function() {

  $('.edit-modal').on('click', function(){
    
      $('#editModalClientes').modal('show');

      $tr = $(this).closest('tr');
      var data = $tr.children("td").map(function(){
          return $(this).text();
      }).get();
      console.log(data);

      $('#id').val(data[0]);
      $('#cedula').val(data[1]);
      $('#ruc').val(data[2]); 
      $('#name').val(data[3]); 

  });

  $('#editClientes').on('submit', function(e){
    e.preventDefault();
    var id = $('#id').val();
    $.ajax({
      type: "PUT",
      url:'clientes/updateClientes/'+id,
      data: $('#editClientes').serialize(),
      success: function(response){
        console.log(response);
        $('#editModalClientes').modal('hide');
        alert("Actualizacion..");
        location.reload();
      },
      error: function(error){
        console.log(error);
      }
    });
  });
});
    
</script>