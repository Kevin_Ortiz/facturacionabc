<!-- Button trigger modal -->

  <!-- Modal create clientes -->
  <div class="modal fade" id="addModalClientes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Registra Nuevo Cliente</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
       <form id="addCliente">
            <div class="modal-body">
                {{ csrf_field() }}
                
                <div class="form-group">
                    <label>Cedula:</label>
                    <input type="text" class="form-control" name="cedula" placeholder="ingrese su numero de cedula" required>
                </div>

                <div class="form-group">
                    <label>ruc:</label>
                    <input type="text" class="form-control" name="ruc" placeholder="ingrese su ruc" required>
                </div>

                <div class="form-group">
                    <label>Nombre:</label>
                    <input type="text" class="form-control" name="name" placeholder="ingrese su nombre" required>
                </div>

               

             </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
      </form>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script type="text/javascript">

    $(document).ready(function() {

        $('#addCliente').on('submit', function(e){
            e.preventDefault();

            $.ajax({
                type:"POST",
                url: 'clientes/store',
                data: $('#addCliente').serialize(),
                success: function (response) {
                    console.log(response)
                    $('#msj-success').modal('hide')
                    alert("Guardado...");
                    location.reload();
                },
                error: function(err){
                    console.log(err);
                    alert(JSON.stringify(err,null, 2).replace(/{/g, '').replace(/}/g, '')
                    .replace(/""/g,'').replace(/"readyState"/g,'').replace(/,/g,'').replace(/^ +/gm, '')
                    .replace(/]/g,).replace(/\"/gm, ''));
                }
            });
        });
    });
</script>
