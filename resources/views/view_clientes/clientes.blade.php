@extends('layouts.app')

@section('content')

<div class="row">

    <div class="col-lg-3"></div>

    <div class="col-lg-8">
        <h1 class="text-center p-3">Mantenimiento clientes</h1>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModalClientes">Nuevo</button>
        <table class="table table-border  ">
            <thead>
                <tr>
                    <td>id</td>
                    <td>cedula</td>
                    <td>ruc</td>
                    <td>nombre</td>
                    <td>Acciones</td>
                </tr>
            </thead>
            <tbody>
                @foreach($cliente as $cliente)
                    <tr>
                        <td>{{ $cliente->id }}</td>
                        <td>{{ $cliente->cedula }}</td>
                        <td>{{ $cliente->ruc }}</td>
                        <td>{{ $cliente->name }}</td>
                        <td>
                            <a href="#" class="edit-modal btn btn-success" data-id="{{ $cliente->id }}"
                                data-toggle="modal" data-target="#editModalClientes">Actulizar</a>

                            <a href="#" class="delete-modal btn btn-danger" data-id="{{ $cliente->id }}"
                                data-toggle="modal" data-target="#deleteModalClientes">Eliminar</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>
@include('view_clientes.Createmodal');
@include('view_clientes.editmodal');
@include('view_clientes.deletemodal');

@endsection
