@extends('layouts.app')

@section('content')

<div class="row">

    <div class="col-lg-3"></div>

    <div class="col-lg-8 p-3">
        <h1 class="text-center p-3">Configuracion de Factura</h1>
        <br>
        <form id="IdConfiguracion ">

            {{ csrf_field() }}
            {{ method_field('PUT') }}

            @foreach($config as $config)

                <label for="">Establecimiento:</label>

                <input type="hidden" name="id" id="id" value="{{ $config->id }}">

                <input type="text" name="establecimiento" id="establecimiento" value="{{ $config->establecimiento }}"
                    required>

                <label for="">Punto de Emision:</label>
                <input type="text" name="punto_emision" id="punto_emision" value="{{ $config->punto_emision }}"
                    required>

                <label for="">Sec factura:</label>
                <input type="text" name="sec_factura" id="sec_factura" value="{{ $config->sec_factura }}" required>

            @endforeach
            <div class="content mt-3 text-center p-4" >
                <button type="submit" class="btn btn-primary ">Actualizar</button>
            </div>
        </form>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $('#IdConfiguracion').on('submit', function (e) {
            e.preventDefault();
            var id = $('#id').val();
            $.ajax({
                type: "PUT",
                url: 'configuracion/updateConfig/' + id,
                data: $('#IdConfiguracion').serialize(),
                success: function (response) {
                    console.log(response);
                    //  $('#editModalEmpresas').modal('hide');
                    alert("Actualizacion..");
                    location.reload();
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
    });

</script>

@endsection
