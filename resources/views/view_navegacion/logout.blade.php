<li class="nav-item dropdown">
    <li class="nav-item dropdown">
        <a id="navbarDropdown" class="nav-link " href="#" role="button"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            {{ Auth::user()->name }} 
        </a>

        <li>
            <a href="{{ url('/logout') }}" class="nav-link "
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                salir <i class="fas fa-sign-out-alt"></i>
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </div>
</li>