

<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <!-- Add icons to the links using the .nav-icon class
         with font-awesome or any other icon font library -->
         
    <li class="nav-item has-treeview menu-open">
      <a href="#" class="nav-link active">
          <i class="fas fa-file-invoice"></i>
        <p>
          Opciones de Facturación
          <i class="right fas fa-angle-left"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">

      @include('view_navegacion.nav.' . \App\User::navigation()) 
     

      </ul>
    </li>
    
  </ul>

