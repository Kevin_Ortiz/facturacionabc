


<li class="nav-item">
<a href="{{url('/clientes')}}" class="nav-link active">
      <i class="far fa-circle nav-icon"></i>
      <p>Mantenimiento Clientes</p>
    </a>
  </li>
  <li class="nav-item">
    <a href="{{url('/empresas')}}" class="nav-link active">
      <i class="far fa-circle nav-icon"></i>
      <p>Mantenimiento Empresa</p>
    </a>
  </li>
  <li class="nav-item">
    <a href="{{url('/productos')}}" class="nav-link active">
      <i class="far fa-circle nav-icon"></i>
      <p>Mantenimiento Productos</p>
    </a>
  </li>
  <li class="nav-item">
    <a href="{{url('/configuracion')}}" class="nav-link active">
      <i class="far fa-circle nav-icon"></i>
      <p>Configuración</p>
    </a>
  </li>
  <li class="nav-item">
    <a href="{{url('/EmitirFactura')}}" class="nav-link">
      <i class="far fa-circle nav-icon"></i>
      <p>Emitir Factura</p>
    </a>
  </li>

  <li class="nav-item">
    <a href="#" class="nav-link">
      <i class="far fa-circle nav-icon"></i>
      <p>Consultar Facturas</p>
    </a>
  </li>